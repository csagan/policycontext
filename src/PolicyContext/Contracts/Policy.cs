﻿using System;

namespace PolicyContext.Contracts
{
    public class Policy
    {
        public Guid Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Number { get; set; }
    }
}
